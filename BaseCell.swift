//
//  File.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/23/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    
    func setupViews() {
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
