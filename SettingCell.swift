//
//  SettingCell.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/26/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit

class SettingCell: BaseCell {
    
    
    var setting: SettingOptions? {
        didSet {
            nameLabel.text = setting?.name
            
            if let imageName = setting?.imageName {
                iconImageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
                iconImageView.tintColor = UIColor.darkGray
            }
        }
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    
    }()
    
    override func setupViews() {
        super.setupViews()

        addSubview(nameLabel)
        addSubview(iconImageView)
        
        addConstraintsWith(format: "H:|-8-[v0(30)]-8-[v1]|", view: iconImageView, nameLabel)
        addConstraintsWith(format: "V:|[v0]|", view: nameLabel)
        addConstraintsWith(format: "V:[v0(30)]", view: iconImageView)
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))

    }
    
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white
            nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.darkGray
            iconImageView.tintColor = isHighlighted ? UIColor.white : UIColor.darkGray
        }
    }
    
}























