//
//  SettingsLauncher.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/26/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit


class SettingOptions: NSObject {
    let name: String
    let imageName: String
    
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
    }
    
}

class SettingsLauncher: NSObject, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let cellHeight: CGFloat = 50
    let blackView = UIView()
    
    let collectionView: UICollectionView = {
        let cv =  UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let cellId = "cellId"
    
    let settings: [SettingOptions] = {
        return [SettingOptions(name: "Settings", imageName: "settings"), SettingOptions(name: "Terms & privacy policy", imageName: "privacy"), SettingOptions(name: "Feedback", imageName: "feedback"), SettingOptions(name: "Help", imageName: "help"), SettingOptions(name: "Switch Account", imageName: "switch_account"), SettingOptions(name: "Cancel", imageName: "cancel")]
    }()
    
    weak var homeController: HomeController?
    
    
    func showSettings() {
        // show menu
        
        // dim screen "under" the menu
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(blackView)
            window.addSubview(collectionView)
            
            let height: CGFloat  = CGFloat(settings.count) * cellHeight
            let y = window.frame.height - height
            
            collectionView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.frame = window.frame
            blackView.alpha = 0

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectionView.frame.origin.y = y
            }, completion: nil)
        }
    }
    
    func handleDismiss(setting: SettingOptions) {
        // dimiss settings collection view
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: { [weak self]  in
            self?.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self?.collectionView.frame.origin.y = window.frame.height
            }
        }) { [weak self] (completed: Bool) in
            // show controller based on setting selected
            
            if setting.name != "Cancel" {
                self?.homeController?.showController(for: setting)
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! SettingCell
        
        let setting = settings[indexPath.item]
        cell.setting = setting
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let setting = settings[indexPath.item]
        handleDismiss(setting: setting)
    }
    
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    override init() {
        super.init()
        // do something here
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SettingCell.self, forCellWithReuseIdentifier: "cellId")
    
    }
    
    
    
}
















