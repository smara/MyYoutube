//
//  ViewController.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/21/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    
    let menuBar: MenuBar = {
        let mb = MenuBar()
        return mb
    }()
    
    var videos: [Video]?
//        = {
//        
//        var kanyeChannel = Channel()
//        kanyeChannel.name = "KanyeIsTheBestChannel"
//        kanyeChannel.profileImageName = "kanye_profile"
//        
//        
//        var blankSpaceVideo = Video()
//        blankSpaceVideo.title = "Taylor Swift - Blank Space"
//        blankSpaceVideo.thumbnailImageName = "taylor_swift_blank_space"
//        blankSpaceVideo.channel = kanyeChannel
//        blankSpaceVideo.numberOfViews = 2809737891
//        
//        var badBloodVideo = Video()
//        badBloodVideo.title = "Taylor Swift - Bad Blood featuring Kendrick Lamar"
//        badBloodVideo.thumbnailImageName = "taylor_swift_bad_blood"
//        badBloodVideo.channel = kanyeChannel
//        badBloodVideo.numberOfViews = 4986032647
//        
//        return [blankSpaceVideo, badBloodVideo]
//    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchVideos()
        navigationItem.title = "Home"
        navigationController?.navigationBar.isTranslucent = false
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: 20))
        titleLabel.text = "Home"
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(VideoCell.self, forCellWithReuseIdentifier: "videoCellId")
        collectionView?.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        
        setupMenuBar()
        setupNavBarButtons()
    }
    
    func fetchVideos() {
       
//        let rawUrl: String = "https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics&chart=mostPopular&fields=items(id,snippet(channelId,channelTitle,publishedAt,tags,thumbnails/default,title),statistics(likeCount,viewCount))&key=AIzaSyA983Qcgih38n5vazCi9nMTvrDwIBhPrV4"
////        let urlstring = rawUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        let request = URLRequest(url: URL(string: rawUrl)!)
//        
//        URLSession.shared.dataTask(with: request) { (data, response, error) in
//            
//            if error != nil {
//                print(error!)
//                return
//            }
//            do {
//                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String:Any],
//                    let items = json["items"] as? [[String:Any]] {
//                    
//                    for dictionary in items {
//                        print(dictionary["id"]!)
//                    }
//                }
//            } catch let jsonError {
//                print(jsonError)
//            }
//        }.resume()

        
        let url = URL(string: "https://s3-us-west-2.amazonaws.com/youtubeassets/home.json")
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                
                self.videos = [Video]()
                
                for dictionary in json as! [[String:AnyObject]] {
                    let video = Video()
                    video.title = dictionary["title"] as? String
                    video.thumbnailImageName = dictionary["thumbnail_image_name"] as? String
                    
                    
                    let channelDict = dictionary["channel"] as! [String:AnyObject]
                    
                    let channel = Channel()
                    video.channel = channel
                    channel.name = channelDict["name"] as? String
                    channel.profileImageName = channelDict["profile_image_name"] as? String
                    
                    
                    self.videos?.append(video)
                }
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
    }
    
    
    func setupNavBarButtons() {
        let searchImage = UIImage(named: "search_icon")?.withRenderingMode(.alwaysOriginal)
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        
        let moreImage = UIImage(named: "nav_more_icon")?.withRenderingMode(.alwaysOriginal)
        let moreBarButtonItem = UIBarButtonItem(image: moreImage, style: .plain, target: self, action: #selector(handleMore))
        
        navigationItem.rightBarButtonItems = [moreBarButtonItem,searchBarButtonItem]
    }

    
    lazy var settingsLauncher: SettingsLauncher = {
        let launcher = SettingsLauncher()
        launcher.homeController = self
        return launcher
    }()
    
    
    func handleMore() {
        settingsLauncher.showSettings()
    }
   
    
    func handleSearch() {
    }
    
    
    func showController(for setting: SettingOptions){
        let dummyController = UIViewController()
        dummyController.view.backgroundColor = UIColor.white
        dummyController.navigationItem.title = setting.name
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.pushViewController(dummyController, animated: true)
    }
    
    private func setupMenuBar(){
        view.addSubview(menuBar)
        view.addConstraintsWith(format: "H:|[v0]|", view: menuBar)
        view.addConstraintsWith(format: "V:|[v0(50)]", view: menuBar)
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos?.count ?? 0
    }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCellId", for: indexPath) as! VideoCell
        
        cell.video = videos?[indexPath.item]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imageWidht = view.frame.width - 16 - 16
        let imageHeight = imageWidht * 9 / 16
        
        let verticalConstraints: CGFloat = 16 + 8 + 4  + 16 + 20
        let otherViewsHeight: CGFloat = 20 + 30
        
        return CGSize(width: view.frame.width, height: (imageHeight + otherViewsHeight + verticalConstraints))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}

































