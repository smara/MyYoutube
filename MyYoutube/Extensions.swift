//
//  Extensions.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/21/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import QuartzCore


extension UIView {
    
    // ... means array
    func addConstraintsWith(format: String, view: UIView...) {
        var viewsDictionary = [String : UIView]()
        for (index, view) in view.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}


extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}



class CustomImageView: UIImageView {

    let imageCache = NSCache<NSString, UIImage>()
    var imageURLString: String?
    
    
    
    
    func loadImageUsing(urlString: String) {

        imageURLString = urlString

        let url = URL(string: urlString)
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: NSString(string: urlString)) {
            self.image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                if self.imageURLString == urlString {
                    self.imageCache.setObject(imageToCache!, forKey: NSString(string: urlString))
                }
                self.image = imageToCache
            }
        }).resume()
    }
}
































