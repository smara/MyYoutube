//
//  Video.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/23/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit


class Video: NSObject {
    
    var thumbnailImageName: String?
    var title: String?
    var numberOfViews: NSNumber?
    var uploadDate: NSDate?
    
    var channel: Channel?
}



class Channel: NSObject {
    
    var name: String?
    var profileImageName: String?
    
}
