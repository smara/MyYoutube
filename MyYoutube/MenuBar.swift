//
//  MenuBar.swift
//  MyYoutube
//
//  Created by Silvia Florido on 3/23/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit

class MenuBar: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = UIColor.rgb(red: 230, green: 32, blue: 31)
        return cv
    }()
    
    let menuBarCellId = "menuBarCellId"
    
    let imageNames = ["home","trending","subscriptions","account"]
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(collectionView)
        
        // attached to underlying view contraints set on HomeControler.setupMenuBar(). Height = 50
        addConstraintsWith(format: "H:|[v0]|", view: collectionView)
        addConstraintsWith(format: "V:|[v0]|", view: collectionView)
        
        collectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: menuBarCellId)
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuBarCellId, for: indexPath) as! MenuBarCell
        
        cell.imageView.image = UIImage(named: imageNames[indexPath.item])?.withRenderingMode(.alwaysTemplate)
//        cell.imageView.tintColor = UIColor.rgb(red: 91, green: 14, blue: 13)
        cell.tintColor = UIColor.rgb(red: 91, green: 14, blue: 13)
        return cell
    }
    
    
    // MARK: UICollectionViewDelegate
    
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/4, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}




















